import User from "App/Models/User";
import SaveUserValidator from "App/Validators/SaveUserValidator";

export default class UsersController {
  public async createUser({ request, response }) {
    try {
      const { username, email, password } = await request.validate(
        SaveUserValidator
      );
      if (username === undefined) {
        throw { code: "FIELD_UNDEFINED", field: "username" };
      } else if (email === undefined) {
        throw { code: "FIELD_UNDEFINED", field: "email" };
      } else if (password === undefined) {
        throw { code: "FIELD_UNDEFINED", field: "password" };
      } else {
        const savedUser = await User.create({
          username,
          email: email.toLowerCase(),
          password,
        });
        response.status(201).json(savedUser);
      }
    } catch (error) {
      if (error.constraint === "users_email_unique") {
        throw new Error("ER_DUP_ENTRY");
      } else {
        throw error;
      }
    }
  }

  public async getAllUsers({ response }) {
    const users = await User.all();
    response.status(200).json(users);
  }

  public async getUser({ response, params, auth }) {
    const { id } = params;
    const loginUserId = await auth.user.id;

    if (loginUserId === id) {
      const user = await User.findOrFail(id);
      response.status(200).json(user);
    } else {
      throw new Error("Unauthorized access");
    }
  }

  public async updateUser({ request, response, params, auth }) {
    const { id } = params;
    const loginUserId = await auth.user.id;

    if (loginUserId === id) {
      try {
        const { username, email, password } = await request.validate(
          SaveUserValidator
        );

        if (username === undefined) {
          throw { code: "FIELD_UNDEFINED", field: "username" };
        } else if (email === undefined) {
          throw { code: "FIELD_UNDEFINED", field: "email" };
        } else if (password === undefined) {
          throw { code: "FIELD_UNDEFINED", field: "password" };
        } else {
          const existedUser = await User.findOrFail(id);
          existedUser.username = username;
          existedUser.email = email;
          existedUser.password = password;
          const updatedUser = await existedUser.save();
          response.status(200).json(updatedUser);
        }
      } catch (error) {
        if (error.constraint === "users_email_unique") {
          throw new Error("ER_DUP_ENTRY");
        } else {
          throw error;
        }
      }
    } else {
      throw new Error("Unauthorized access");
    }
  }

  public async deleteUser({ response, params, auth }) {
    const { id } = params;
    const loginUserId = auth.user.id;

    if (loginUserId === id) {
      const user = await User.findOrFail(id);
      await user.delete();
      response.status(200).json({
        message: `User with id: ${id} deleted successfully`,
        timestam: new Date(),
      });
    } else {
      throw new Error("Unauthorized access");
    }
  }

  public async getCurrentLoginUser({ response, auth }) {
    const loginUser = await auth.user;
    response.status(200).json(loginUser);
  }
}
