import Database from "@ioc:Adonis/Lucid/Database";
import Expense from "App/Models/Expense";
import Friend from "App/Models/Friend";
import AddExpenseValidator from "App/Validators/AddExpenseValidator";

export default class ExpensesController {
  public async addExpense({ request, response, params, auth }) {
    const { id } = params;
    const friendData = await Friend.findOrFail(id);
    const loginUserId = auth.user.id;

    if (friendData.userId === loginUserId) {
      const { description, amount } = await request.validate(
        AddExpenseValidator
      );

      if (description === undefined) {
        throw { code: "FIELD_UNDEFINED", field: "description" };
      } else if (amount === undefined) {
        throw { code: "FIELD_UNDEFINED", field: "amount" };
      } else {
        const expense = await Expense.create({
          friendId: friendData.id,
          description,
          amount,
        });
        return response.status(201).json(expense);
      }
    } else {
      throw new Error("Unauthorized access");
    }
  }

  public async deleteExpense({ response, params, auth }) {
    const { id } = params;
    const loginUserId = await auth.user.id;
    const expense = await Expense.findOrFail(id);
    const friendData = await Friend.findOrFail(expense.friendId);

    if (friendData.userId === loginUserId) {
      await expense.delete();
      response.status(200).json({
        message: `Expense with id: ${id} deleted successfully`,
        timestamp: new Date(),
      });
    } else {
      throw new Error("Unauthorized access");
    }
  }
  public async getAllExpensesByFriend({ response, params, auth }) {
    const { id } = params;
    const loginUserId = auth.user.id;
    const friendData = await Friend.findOrFail(id);

    if (friendData.userId === loginUserId) {
      const expenseList = await Database.from("expenses").where(
        "friend_id",
        id
      );
      response.status(200).json(expenseList);
    } else {
      throw new Error("Unauthorized access");
    }
  }
}
