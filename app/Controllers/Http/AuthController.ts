import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class AuthController {
  public async login({ request, response, auth }: HttpContextContract) {
    const email = request.input("email");
    const password = request.input("password");

    if (email === undefined || password === undefined || email.trim()==='' || password.trim()==='') {
      return response.status(400).json({
        message: "email and password required",
        timestamp: new Date(),
      });
    } else {
      const token = await auth
        .use("api")
        .attempt(email.toLowerCase(), password, {
          expiresIn: "10 days",
        });
      return token.toJSON();
    }
  }
  public async logout({response,auth}){
    await auth.use('api').revoke();
    response.status(200).json({
      revoked:true
    })
  }
}
