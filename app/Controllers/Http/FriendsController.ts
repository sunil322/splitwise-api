import Database from "@ioc:Adonis/Lucid/Database";
import Friend from "App/Models/Friend";
import AddFriendValidator from "App/Validators/AddFriendValidator";

export default class FriendsController {
  public async addFriend({ request, response, auth }) {
    const loginUserId = await auth.user.id;
    const { name } = await request.validate(AddFriendValidator);

    if (name === undefined) {
      throw { code: "FIELD_UNDEFINED", field: "friendName" };
    } else {
      const friend = await Friend.create({
        userId: loginUserId,
        name,
      });
      response.status(201).json(friend);
    }
  }

  public async getAllFriendsByUser({ response, auth }) {
    const loginUserId = await auth.user.id;
    const friendsList = await Database.from("friends").where(
      "user_id",
      loginUserId
    );

    response.status(200).json(friendsList);
  }
}
