import Logger from "@ioc:Adonis/Core/Logger";
import HttpExceptionHandler from "@ioc:Adonis/Core/HttpExceptionHandler";

export default class ExceptionHandler extends HttpExceptionHandler {
  constructor() {
    super(Logger);
  }
  public async handle(error: any, { response, params }): Promise<any> {
    if (error.code === "E_ROW_NOT_FOUND") {
      const { id } = params;
      response.status(404).json({
        message: `No data found with id: ${id}`,
        timestamp: new Date(),
      });
    } else if (
      error.code === "E_UNAUTHORIZED_ACCESS" ||
      error.message === "Unauthorized access"
    ) {
      response.status(401).json({
        message: `Unauthorized access`,
        timestamp: new Date(),
      });
    } else if (error.code === "E_ROUTE_NOT_FOUND") {
      response.status(404).json({
        message: "The api you are looking for doesn't exist",
        timestamp: new Date(),
      });
    } else if (error.message === "ER_DUP_ENTRY") {
      response.status(409).json({
        message: "Email already registered",
        timestamp: new Date(),
      });
    } else if (error.code === "E_VALIDATION_FAILURE") {
      const allValidationMessages = error.messages.errors.reduce(
        (
          allValidationErrors: Object[],
          error: { field: string; message: string }
        ) => {
          const validationMessage = {};
          (validationMessage["field"] = error.field),
            (validationMessage["message"] = error.message);
          allValidationErrors.push(validationMessage);
          return allValidationErrors;
        },
        []
      );

      response.status(400).json({
        message: allValidationMessages,
        timestamp: new Date(),
      });
    } else if (error.code === "FIELD_UNDEFINED") {
      response.status(400).json({
        message: [
          {
            field: `${error.field}`,
            message: `${error.field} is required`,
          },
        ],
        timestamp: new Date(),
      });
    } else if (error.code === "E_INVALID_AUTH_UID") {
      response.status(401).json({
        message: "Invalid email",
        timestamp: new Date(),
      });
    } else if (error.code === "E_INVALID_AUTH_PASSWORD") {
      response.status(401).json({
        message: "Invalid password",
        timestamp: new Date(),
      });
    } else {
      response.status(500).json({
        message: "Something went wrong",
        timestamp: new Date(),
      });
    }
  }
}
