import { schema, CustomMessages,rules } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class SaveUserValidator {
  constructor(protected ctx: HttpContextContract) {}

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    username:schema.string({trim:true},[
      rules.trim(),
      rules.minLength(2),
      rules.maxLength(15)
    ]),
    email:schema.string({trim:true},[
      rules.trim(),
      rules.email()
    ]),
    password:schema.string({trim:true},[
      rules.trim(),
      rules.minLength(8),
      rules.maxLength(16),
      rules.regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
    ])
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages: CustomMessages = {
    "required": "{{field}} is required",
    "username.minLength": "username should be at least of 2 characters",
    "username.maxLength": "username should be maximum of 15 charcters",
    "email.email": "invalid email address",
    "password.minLength": "password should be at least of 8 characters",
    "password.maxLength": "password should be maximum of 16 characters",
    "password.regex":"Password should consists of at least one uppercase letter, one lowercase letter, one number and one special character"
  }
}
