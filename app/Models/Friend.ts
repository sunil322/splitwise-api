import { BaseModel, HasMany, column, hasMany } from "@ioc:Adonis/Lucid/Orm";
import Expense from "./Expense";

export default class Friend extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column({ columnName: "name" })
  public name: string;

  @column({ columnName: "user_id" })
  public userId: string;

  @hasMany(() => Expense)
  public expenses: HasMany<typeof Expense>;
}
