import { DateTime } from "luxon";
import {
  BaseModel,
  HasMany,
  beforeCreate,
  beforeSave,
  column,
  hasMany,
} from "@ioc:Adonis/Lucid/Orm";
import Hash from "@ioc:Adonis/Core/Hash";
import { v4 as uuidv4 } from "uuid";
import Friend from "./Friend";

export default class User extends BaseModel {
  @column({ columnName: "id", isPrimary: true })
  public id: string;

  @column({ columnName: "username" })
  public username: string;

  @column({ columnName: "email" })
  public email: string;

  @column({ columnName: "password", serializeAs: null })
  public password: string;

  @hasMany(() => Friend)
  public friends: HasMany<typeof Friend>;

  @column.dateTime({ columnName: "created_at", autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({
    columnName: "updated_at",
    autoCreate: true,
    autoUpdate: true,
  })
  public updatedAt: DateTime;

  @beforeCreate()
  public static assignUuid(user: User) {
    user.id = uuidv4();
  }

  @beforeSave()
  public static async hashPassword(user: User) {
    user.password = await Hash.make(user.password);
  }
}
