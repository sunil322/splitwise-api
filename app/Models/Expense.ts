import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Expense extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column({columnName:'friend_id'})
  public friendId:number;

  @column({columnName:'description'})
  public description:string;

  @column({columnName:'amount'})
  public amount:number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

}
