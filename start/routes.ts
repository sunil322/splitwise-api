import Route from "@ioc:Adonis/Core/Route";

Route.post("/login", "AuthController.login");

Route.post("/", "UsersController.createUser").prefix("/users");

Route.group(() => {

  Route.get("/current","UsersController.getCurrentLoginUser");
  Route.post("/logout","AuthController.logout");

  Route.group(() => {
    Route.get("/", "UsersController.getAllUsers");
    Route.get("/:id", "UsersController.getUser");
    Route.put("/:id", "UsersController.updateUser");
    Route.delete("/:id", "UsersController.deleteUser");
  }).prefix("/users");

  Route.group(() => {
    Route.post("/", "FriendsController.addFriend");
    Route.get("/", "FriendsController.getAllFriendsByUser");
  }).prefix("/friends");

  Route.group(() => {
    Route.post("/:id", "ExpensesController.addExpense");
    Route.delete("/:id", "ExpensesController.deleteExpense");
    Route.get('/:id',"ExpensesController.getAllExpensesByFriend")
  }).prefix("/expenses");

}).middleware("auth:api");
